<?php

namespace App\Repository;

use App\Entity\Book;
use App\Entity\SearchBook;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * @method Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Book[]    findAll()
 * @method Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookRepository extends ServiceEntityRepository
{

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Book::class);
        $this->paginator = $paginator;
    }

    public function findAll()
    {
        return $this->findBy(array(), array('id' => 'DESC'));
    }

    /**
     *
     * @return PaginationInterface
     *
     */
    public function findSearch(SearchBook $search): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('b')
            ->select('b', 'c', 'a')
            ->join('b.category', 'c')
            ->join('b.author', 'a');

        if (!empty($search->search)) {
            $query = $query
                ->andWhere('b.title LIKE :search')
                ->setParameter('search', "%{$search->search}%");
        }

        if (!empty($search->publishedAt)) {
            $query = $query
                ->andWhere('YEAR(b.release_date) LIKE :publishedAt')
                ->setParameter('publishedAt', "%{$search->publishedAt}%");
        }

        if (!empty($search->categories)) {
            $query = $query
                ->andWhere('c.id IN (:categories)')
                ->setParameter('categories', $search->categories);
        }

        if (!empty($search->authors)) {
            $query = $query
                ->andWhere('a.id IN (:authors)')
                ->setParameter('authors', $search->authors);
        }

        if (!empty($search->isAvailable)) {
            $query = $query
                ->andWhere('b.isAvailable IN (:isAvailable)')
                ->setParameter('isAvailable', $search->isAvailable);
        }

        $query = $query->getQuery();
        return $this->paginator->paginate(
            $query,
            $search->page,
            6
        );
    }

    public function findAllRequestBook($id)
    {
        return $this->createQueryBuilder('r')
            ->join('r.account', 'a')
            ->where('r.isRequested = 1')
            ->andWhere('r.account = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        ;
    }

    public function findAllRequestBooks()
    {
        return $this->createQueryBuilder('r')
            ->join('r.account', 'a')
            ->join('a.user', 'u')
            ->where('r.isRequested = 1')
            ->getQuery()
            ->getResult();
        ;
    }

    public function findAllConfirmBook($id)
    {
        return $this->createQueryBuilder('c')
            ->join('c.account', 'a')
            ->where('c.isConfirmRequestedBook = 1')
            ->andWhere('c.account = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        ;
    }

    public function countAllDelayedBooks($id)
    {
        $now = new \DateTime('now');

        return $this->createQueryBuilder('d')
            ->select('count(d)')
            ->join('d.account', 'a')
            ->where('d.returnedAt < CURRENT_DATE()')
            ->andWhere('d.account = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleScalarResult();
        ;
    }

    public function findAllConfirmBooks()
    {
        return $this->createQueryBuilder('c')
            ->join('c.account', 'a')
            ->join('a.user', 'u')
            ->where('c.isConfirmRequestedBook = 1')
            ->orderBy('c.borrowedAt', 'ASC')
            ->getQuery()
            ->getResult();
        ;
    }

    public function countAllRequestBooks()
    {
        return $this->createQueryBuilder('c')
                ->select('count(c.isRequested)')
                ->where('c.isRequested = 1')
                ->getQuery()
                ->getSingleScalarResult();
        ;
    }
}
