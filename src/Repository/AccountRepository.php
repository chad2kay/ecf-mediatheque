<?php

namespace App\Repository;

use App\Entity\Account;
use App\Entity\SearchUser;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;

/**
 * @method Account|null find($id, $lockMode = null, $lockVersion = null)
 * @method Account|null findOneBy(array $criteria, array $orderBy = null)
 * @method Account[]    findAll()
 * @method Account[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountRepository extends ServiceEntityRepository
{

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(
        ManagerRegistry $registry,
        PaginatorInterface $paginator
    ) {
        parent::__construct($registry, Account::class);
        $this->paginator = $paginator;
    }

    public function countisInactive()
    {
        return $this->createQueryBuilder('z')
            ->select('count(z.isActive)')
            ->where('z.isActive = 0')
            ->getQuery()
            ->getSingleScalarResult();
        ;
    }
    public function countisActive()
    {
        return $this->createQueryBuilder('z')
            ->select('count(z.isActive)')
            ->where('z.isActive = 1')
            ->getQuery()
            ->getSingleScalarResult();
        ;
    }
    
    public function findAllActiveAccount()
    {
        return $this->createQueryBuilder('a')
            ->join('a.user', 'u')
            ->where('a.isActive = 1')
            ->getQuery()
            ->getResult();
        ;
    }

    public function findAllActiveAccounts($id)
    {
        return $this->createQueryBuilder('a')
            ->join('a.user', 'u')
            ->andWhere('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        ;
    }

    public function findAllInactiveAccounts()
    {
        return $this->createQueryBuilder('i')
            ->join('i.user', 'u')
            ->where('i.isActive = 0')
            ->getQuery()
            ->getResult();
        ;
    }

    public function findAllInactiveAccount($id)
    {
        return $this->createQueryBuilder('i')
            ->join('i.user', 'u')
            ->where('i.isActive = 0')
            ->andWhere('i.user = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        ;
    }

    /**
     *
     * @return PaginationInterface
     *
     */
    public function findSearch(SearchUser $search): PaginationInterface
    {
        $query = $this
            ->createQueryBuilder('a')
            ->join('a.user', 'u')
            ->where('a.isActive = 1');

        if (!empty($search->search)) {
            $query = $query
                ->andWhere('u.lastname LIKE :search')
                ->setParameter('search', "%{$search->search}%");
        }

        $query = $query->getQuery();
        return $this->paginator->paginate(
            $query,
            $search->page,
            20
        );
    }

    // /**
    //  * @return Account[] Returns an array of Account objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Account
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
