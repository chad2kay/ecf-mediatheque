<?php

namespace App\Service;

use App\Entity\Book;
use App\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class SlugServices
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected $slugger;

    public function __construct(
        EntityManagerInterface $entityManager,
        SluggerInterface $slugger
    ) {
        $this->entityManager = $entityManager;
        $this->slugger = $slugger;
    }

    public function setSlugBook(Book $book)
    {
        $book->setSlug($this->slugger->slug($book->getTitle()));

        $this->entityManager->persist($book);
        $this->entityManager->flush();
    }

    public function setSlugCategory(Category $category)
    {
        $category->setSlug($this->slugger->slug($category->getTitle()));

        $this->entityManager->persist($category);
        $this->entityManager->flush();
    }
}
