<?php

namespace App\Entity;

use App\Entity\Category;

class SearchBook
{

    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $search = '';

    /**
     * @var Category[]
     */
    public $categories = [];


    /**
     * @var Author[]
     */
    public $authors = [];

    /**
     * @var null|integer
     */
    public $publishedAt;

    /**
     * @var boolean
     */
    public $isAvailable = false;
}
