<?php

namespace App\Entity;

use App\Entity\User;

class SearchUser
{

    /**
     * @var int
     */
    public $page = 1;

    /**
     * @var string
     */
    public $search = '';
}
