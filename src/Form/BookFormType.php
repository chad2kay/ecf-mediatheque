<?php

namespace App\Form;

use DateTime;
use App\Entity\Book;
use App\Entity\Author;
use App\Entity\Category;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class BookFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => false,
            ])
            ->add('Description', TextareaType::class, [
                'label' => false,
                'attr' => [
                    'rows' => '10'
                ]
            ])
            ->add('release_date', DateType::class, [
                'label' => false,
                'years' => range(
                    (new DateTime('1900-01-01'))->format('Y'),
                    (new DateTime())->format('Y')
                ),
            ])
            ->add('author', EntityType::class, [
                'label' => false,
                'class' => Author::class,
                'choice_label' => function (Author $author) {
                    return "{$author->getLastname()} {$author->getFirstname()}";
                },
            ])
            ->add('category', EntityType::class, [
                'label' => false,
                'attr' => [
                    'class' => 'd-flex flex-wrap'
                ],
                'class' => Category::class,
                'choice_label' => function (Category $category) {
                    return "{$category->getTitle()}";
                },
                'multiple' => true,
                'expanded' => true
            ])
            ->add('isAvailable', CheckboxType::class, [
                'label' => false,
                'required' => false
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'label' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
