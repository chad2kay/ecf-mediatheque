<?php

namespace App\Controller;

use App\Repository\BookRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserDashboardController extends AbstractController
{
    /**
     * @Route("/mon-compte", name="user_dashboard")
     */
    public function index(BookRepository $bookRepository): Response
    {
        $data = $this->getUser();
        $currentuser = $this->getUser()->getAccount()->getId();

        $requestedBooks = $bookRepository->findAllRequestBook($currentuser);
        $confirmedBooks = $bookRepository->findAllConfirmBook($currentuser);
        $delayedbooks = $bookRepository->countAllDelayedBooks($currentuser);

        return $this->render('user_dashboard/index.html.twig', [
            'delayedbooks' =>$delayedbooks,
            'confirmedbooks' => $confirmedBooks,
            'requestedbooks' => $requestedBooks,
            'data' => $data
        ]);
    }
}
