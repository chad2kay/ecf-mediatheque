<?php

namespace App\Controller;

use App\Repository\BookRepository;
use App\Repository\AccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccountController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/emprunt/{id}", name="request_book")
     */
    public function requestBook($id, BookRepository $bookRepository): Response
    {
        $currentUser = $this->getUser()->getAccount();

        $requestBook = $bookRepository->find($id);

        $now = new \DateTime('now');
        $maxDayRequest = new \DateTime('+3 day');

        $requestBook->setIsAvailable(false);
        $requestBook->setAccount($currentUser);
        $requestBook->setIsRequested(true);
        $requestBook->setRequestedAt($now);

        $requestBook->setMaxRequestedAt($maxDayRequest);

        $this->entityManager->persist($requestBook);
        $this->entityManager->flush();

        $this->addFlash('success', "Le livre à bien été reservé");
           
        return $this->redirectToRoute('library', [
                'id' => $requestBook->getId(),
            ]);
    }
}
