<?php

namespace App\Controller;

use App\Entity\Author;
use App\Form\AuthorFormType;
use App\Repository\AuthorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AuthorController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected $slugger;

    public function __construct(
        SluggerInterface $slugger,
        EntityManagerInterface $entityManager
    ) {
        $this->slugger = $slugger;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin/auteur", name="admin-author")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function author(
        PaginatorInterface $paginator,
        Request $request,
        AuthorRepository $authorRepository
    ): Response {
        $data = $authorRepository->findAll();

        $authors= $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('admin_dashboard/author/author.html.twig', [
            'authors' => $authors
        ]);
    }

    /**
     * @Route("/admin/auteur/new", name="admin-author-new")
     */
    public function newAuthor(Request $request)
    {
        $author = new Author();

        $form = $this->createForm(AuthorFormType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($author);
            $this->entityManager->flush();
                  
            return $this->redirectToRoute('admin-author');
        }

        return $this->render('admin_dashboard/author/new-author.html.twig', [
            'author' => $author,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/auteur/{id}", name="admin-author-edit", methods="GET|POST")
     * @param Author $author
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function editAuthor(
        Author $author,
        Request $request
    ) {
        $form = $this->createForm(AuthorFormType::class, $author);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($author);
            $this->entityManager->flush();

            return $this->redirectToRoute('admin-author');
        }

        return $this->render('admin_dashboard/author/edit-author.html.twig', [
            'author' => $author,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/author/{id}", name="admin-author-delete", methods="DELETE")
     * @param Author $author
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function deleteAuthor(Author $author, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $author->getId(), $request->get('_token'))) {
            $this->entityManager->remove($author);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin-author');
    }
}
