<?php

namespace App\Controller;

use DateTime;
use App\Entity\Book;
use App\Entity\Admin;
use App\Entity\SearchUser;
use App\Form\BookFormType;
use App\Form\SearchUserFormType;
use App\Service\SlugServices;
use App\Repository\BookRepository;
use App\Repository\UserRepository;
use App\Repository\AccountRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class AdminDashboardController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected $slugger;

    public function __construct(
        SluggerInterface $slugger,
        EntityManagerInterface $entityManager
    ) {
        $this->slugger = $slugger;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin", name="admin")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function index(
        AccountRepository $accountRepository,
        BookRepository $bookRepository
    ): Response {
        $inactiveUsers = $accountRepository->countIsInactive();
        $activeUsers = $accountRepository->countIsActive();
        $requestedBooks = $bookRepository->countAllRequestBooks();
        $confirmRequestedBooks = $bookRepository->findAllConfirmBooks();

        return $this->render('admin_dashboard/index.html.twig', [
            'inactive' => $inactiveUsers,
            'active' => $activeUsers,
            'requestedbook' => $requestedBooks,
            'confirmrequestedbooks' => $confirmRequestedBooks
        ]);
    }

    /**
     * @Route("/admin/comptes-actifs", name="active-accounts")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function activeAccount(
        AccountRepository $accountRepository,
        UserRepository $userRepository,
        Request $request
    ): Response {
        $searchUser = new SearchUser();
        $searchUser->page = $request->get('page', 1);

        $form = $this->createForm(SearchUserFormType::class, $searchUser);

        $form->handleRequest($request);

        $activeAccounts = $accountRepository->findSearch($searchUser);

        if ($request->get('ajax')) {
            return new JsonResponse([
                'content' => $this->renderView('admin_dashboard/_users.html.twig', [
                    'activeaccount' => $$activeAccounts]),
                'pagination' => $this->renderView('admin_dashboard/_pagination.html.twig', [
                    'activeaccounts' => $activeAccounts])
            ]);
        }

        return $this->render('admin_dashboard/active_accounts.html.twig', [
            'activeaccount' => $activeAccounts,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/reservation-en-attente", name="pending_request")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function pendingRequest(AccountRepository $accountRepository): Response
    {
        $requestedBook = $accountRepository->countAllRequestBooks();

        return $this->render('admin_dashboard/active_accounts.html.twig', [
            'requestedBook' => $requestedBook
        ]);
    }

    /**
     * @Route("/admin/reservation/{id}", name="requested_book")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function requestedBook(
        int $id,
        BookRepository $bookRepository,
        AccountRepository $accountRepository
    ): Response {
        $confirmedBooks = $bookRepository->findAllConfirmBook($id);
        $activeAccounts = $accountRepository->findAllActiveAccounts($id);

        return $this->render('admin_dashboard/requested_book.html.twig', [
            'requestedbook' => $confirmedBooks,
            'activeaccount' => $activeAccounts
        ]);
    }

    /**
     * @Route("/admin/reservations", name="requested_books")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function requestedBooks(
        BookRepository $bookRepository
    ): Response {
        $requestedBooks = $bookRepository->findAllRequestBooks();

        return $this->render('admin_dashboard/requested_books.html.twig', [
            'requestedbooks' => $requestedBooks
        ]);
    }

    /**
     * @Route("/admin/comptes-inactifs/", name="inactive-accounts")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function inactiveAccount(
        Request $request,
        AccountRepository $accountRepository
    ): Response {
        $inactiveaccounts = $accountRepository->findAllInactiveAccounts();

        return $this->render('admin_dashboard/inactive_accounts.html.twig', [
            'inactiveaccounts' => $inactiveaccounts
        ]);
    }

    /**
     * @Route("/admin/confirmation-utilisateur/{id}", name="confirm_users")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function confirmAccount(
        int $id,
        Request $request,
        AccountRepository $accountRepository
    ): Response {
        $confirmaccount = $accountRepository->find($id);

        $confirmaccount->setIsActive(true);

        $this->entityManager->persist($confirmaccount);
        $this->entityManager->flush();

        $this->addFlash('confirmeduser', "Le compte à bien été activé");

        return $this->redirectToRoute('inactive-accounts', [
            'confirmaccount' => $confirmaccount
        ]);
    }

    /**
     * @Route("/admin/le-catalogue", name="admin-library")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function library(
        PaginatorInterface $paginator,
        Request $request,
        BookRepository $bookRepository
    ): Response {
        $data = $bookRepository->findAll();

        $books= $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('admin_dashboard/library.html.twig', [
            'books' => $books
        ]);
    }

    /**
     * @Route("/annulation-reservation/{id}", name="remove_request_book")
     */
    public function removeRequestBook(int $id, BookRepository $bookRepository): Response
    {
        $removeRequestBook = $bookRepository->find($id);

        $removeRequestBook->setIsAvailable(true);
        $removeRequestBook->setAccount(null);
        $removeRequestBook->setIsRequested(false);
        $removeRequestBook->setRequestedAt(null);

        $removeRequestBook->setMaxRequestedAt(null);
        $this->entityManager->persist($removeRequestBook);
        $this->entityManager->flush();

        $this->addFlash('success', "La réservation à bien été annulée");
           
        return $this->redirectToRoute('requested_books', [
            'removerequestbook' => $removeRequestBook
            ]);
    }

    /**
     * @Route("/confirmation-retour/{id}", name="confirm_returned_book")
     */
    public function confirmReturnedBook(int $id, BookRepository $bookRepository): Response
    {
        $confirmReturnedBook = $bookRepository->find($id);

        $confirmReturnedBook->setIsAvailable(true);
        $confirmReturnedBook->setAccount(null);
        $confirmReturnedBook->setIsConfirmRequestedBook(null);
        $confirmReturnedBook->setBorrowedAt(null);
        $confirmReturnedBook->setReturnedAt(null);

        $this->entityManager->persist($confirmReturnedBook);
        $this->entityManager->flush();

        $this->addFlash('successReturn', "Retour effectué avec succès");
           
        return $this->redirectToRoute('active-accounts', [
            'confirmreturnedbook' => $confirmReturnedBook->getAccount()
            ]);
    }

    /**
     * @Route("/confirmation-reservation/{id}", name="confirm_request_book")
     */
    public function confirmRequestBook(int $id, BookRepository $bookRepository): Response
    {
        $confirmRequestBook = $bookRepository->find($id);

        $now = new \DateTime('now');
        $returnedbookAt = new \DateTime('+1 month');

        $confirmRequestBook->setIsRequested(false);
        $confirmRequestBook->setRequestedAt(null);
        $confirmRequestBook->setMaxRequestedAt(null);
        $confirmRequestBook->setIsConfirmRequestedBook(true);
        $confirmRequestBook->setBorrowedAt($now);
        $confirmRequestBook->setReturnedAt($returnedbookAt);

        $this->entityManager->persist($confirmRequestBook);
        $this->entityManager->flush();

        $this->addFlash('success', "L'emprunt à bien été confirmée");
           
        return $this->redirectToRoute('requested_books', [
            'removerequestbook' => $confirmRequestBook
            ]);
    }

    /**
     * @Route("/admin/le-catalogue/new", name="admin-library-new")
     */
    public function new(
        SlugServices $slugServices,
        Request $request
    ): Response {
        $book = new Book();
        $form = $this->createForm(BookFormType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $now = new DateTime('now');
            $book->setCreatedAt($now);
            $book->setIsRequested(false);
            
            $slugServices->setSlugBook($book);
        
            return $this->redirectToRoute('admin-library');
        }

        return $this->render('admin_dashboard/new-library.html.twig', [
            'book' => $book,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/le-catalogue/{id}", name="admin-library-edit", methods="GET|POST")
     * @param Book $book
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function edit(
        Book $book,
        SlugServices $slugServices,
        Request $request
    ) {
        $form = $this->createForm(BookFormType::class, $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugServices->setSlugBook($book);
        
            return $this->redirectToRoute('admin-library');
        }

        return $this->render('admin_dashboard/edit-library.html.twig', [
            'book' => $book,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/le-catalogue/{id}", name="admin-library-delete", methods="DELETE")
     * @param Book $book
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function delete(Book $book, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $book->getId(), $request->get('_token'))) {
            $this->entityManager->remove($book);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin-library');
    }
}
