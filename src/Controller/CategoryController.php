<?php

namespace App\Controller;

use App\Entity\Category;
use App\Service\SlugServices;
use App\Form\CategoryFormType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategoryController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    protected $slugger;

    public function __construct(
        SluggerInterface $slugger,
        EntityManagerInterface $entityManager
    ) {
        $this->slugger = $slugger;
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin/categorie", name="admin-category")
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function category(
        PaginatorInterface $paginator,
        Request $request,
        CategoryRepository $categoryRepository
    ): Response {
        $data = $categoryRepository->findAll();

        $categories= $paginator->paginate(
            $data,
            $request->query->getInt('page', 1),
            9
        );

        return $this->render('admin_dashboard/category/category.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("/admin/categorie/new", name="admin-category-new")
     */
    public function newAuthor(
        Request $request,
        SlugServices $slugServices
    ) {
        $category = new Category();

        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugServices->setSlugCategory($category);
                  
            return $this->redirectToRoute('admin-category');
        }

        return $this->render('admin_dashboard/category/new-category.html.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/categorie/{id}", name="admin-category-edit", methods="GET|POST")
     * @param Category $category
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function editAuthor(
        Category $category,
        SlugServices $slugServices,
        Request $request
    ) {
        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $slugServices->setSlugCategory($category);

            return $this->redirectToRoute('admin-category');
        }

        return $this->render('admin_dashboard/category/edit-category.html.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/categorie/{id}", name="admin-category-delete", methods="DELETE")
     * @param Category $category
     * @return \symfony\Component\HttpFoundation\Response
     */
    public function deleteAuthor(Category $category, Request $request)
    {
        if ($this->isCsrfTokenValid('delete' . $category->getId(), $request->get('_token'))) {
            $this->entityManager->remove($category);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('admin-category');
    }
}
