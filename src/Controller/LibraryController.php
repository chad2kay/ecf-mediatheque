<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\SearchBook;
use App\Form\SearchBookFormType;
use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class LibraryController extends AbstractController
{
    /**
     * @Route("/catalogue", name="library")
     */
    public function index(
        BookRepository $bookRepository,
        Request $request
    ): Response {
        $searchBook = new SearchBook();
        $searchBook->page = $request->get('page', 1);

        $form = $this->createForm(SearchBookFormType::class, $searchBook);

        $form->handleRequest($request);
        
        $books = $bookRepository->findSearch($searchBook);

        if ($request->get('ajax')) {
            return new JsonResponse([
                'content' => $this->renderView('library/_book.html.twig', ['books' => $books]),
                'sort' => $this->renderView('library/_sortable.html.twig', ['books' => $books]),
                'pagination' => $this->renderView('library/_pagination.html.twig', ['books' => $books])
            ]);
        }

        return $this->render('library/index.html.twig', [
            'books' => $books,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/catalogue/{slug}", name="book")
     */
    public function book(Book $book): Response
    {
        return $this->render('library/book.html.twig', [
            'book' => $book,
        ]);
    }
}
