<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Admin;
use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;
    protected $slugger;

    public function __construct(
        UserPasswordHasherInterface $passwordHasher,
        SluggerInterface $slugger
    ) {
        $this->passwordHasher = $passwordHasher;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager)
    {
        // use the factory to create a Faker\Generator instance
        $faker = Factory::create('fr_FR');
        //$faker->addProvider(new \Bluemmb\Faker\PicsumPhotosProvider($faker));
        // $product = new Product();
        // $manager->persist($product);

        $admin = new Admin();

        $admin->setEmail('contact@e-mc2.com')
            ->setRoles(['ROLE_ADMIN'])
            ->setisActive(true)
            ->setPassword($this->passwordHasher->hashPassword(
                $admin,
                'password'
            ));
        
        $manager->persist($admin);

        $categories = [
            1 => ['title' => 'Polars'],
            2 => ['title' => 'Littérature française'],
            3 => ['title' => 'Littérature étrangère'],
            4 => ['title' => 'Bande dessinées'],
            5 => ['title' => 'Poches'],
            6 => ['title' => 'Science-fiction'],
            7 => ['title' => 'Informatique'],
            8 => ['title' => 'Romance'],
        ];

        $authors = [];

        for ($i=0; $i < 15; $i++) {
            $author = new Author();

            $author->setFirstname($faker->firstName())
                ->setLastname($faker->lastName());

            $manager->persist($author);

            $authors[] = $author;
        }

        foreach ($categories as $key => $value) {
            $category = new Category();
        
            $category->setTitle($value['title'])
                    ->setSlug($this->slugger->slug($category->getTitle()));
                
            $manager->persist($category);

            for ($i=0; $i < mt_rand(13, 20); $i++) {
                $book = new Book();
            
                $auteur = $faker->randomElement($authors);

                $book->setTitle($faker->words(3, true))
                ->setCreatedAt($faker->dateTimeBetween('-6 month', 'now'))
                ->setDescription($faker->text(250))
                ->setSlug($this->slugger->slug($book->getTitle()))
                //->setImage($faker->imageUrl(280, 350, true))
                ->setImage('cover'.$faker->numberBetween(1, 4).'.jpg')
                ->setReleaseDate($faker->dateTimeBetween('-80 year', 'now'))
                ->setIsAvailable($faker->randomElement([true, false]))
                ->setIsRequested(false)
                ->addCategory($category)
                ->setAuthor($auteur);

                $manager->persist($book);
            }
        }
               
        $manager->flush();
    }
}
