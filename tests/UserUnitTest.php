<?php

namespace App\Tests;

use App\Entity\User;
use DateTime;
use PHPUnit\Framework\TestCase;

class UserUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $user = new User();
        $date = new DateTime();

        $user->setEmail('true@test.com')
            ->setFirstname('firstname')
            ->setLastname('lastname')
            ->setAddress('address')
            ->setBirthdate($date)
            ->setPassword('password');

        $this->assertTrue($user->getEmail() === 'true@test.com');
        $this->assertTrue($user->getFirstName() === 'firstname');
        $this->assertTrue($user->getLastname() === 'lastname');
        $this->assertTrue($user->getAddress() === 'address');
        $this->assertTrue($user->getBirthdate() === $date);
        $this->assertTrue($user->getPassword() === 'password');
    }

    public function testIsFalse()
    {
        $user = new User();
        $date = new DateTime();

        $user->setEmail('true@test.com')
            ->setFirstname('firstname')
            ->setLastname('lastname')
            ->setAddress('address')
            ->setBirthdate($date)
            ->setPassword('password');

        $this->assertFalse($user->getEmail() === 'false@test.com');
        $this->assertFalse($user->getFirstName() === 'false');
        $this->assertFalse($user->getLastname() === 'false');
        $this->assertFalse($user->getAddress() === 'false');
        $this->assertFalse($user->getBirthdate() === new DateTime());
        $this->assertFalse($user->getPassword() === 'false');
    }

    public function testIsEmpty()
    {
        $user = new User();

        $this->assertEmpty($user->getEmail());
        $this->assertEmpty($user->getFirstName());
        $this->assertEmpty($user->getLastname());
        $this->assertEmpty($user->getAddress());
        $this->assertEmpty($user->getBirthdate());
        $this->assertEmpty($user->getPassword());
    }

}
