<?php

namespace App\Tests;

use App\Entity\Admin;
use PHPUnit\Framework\TestCase;

class AdminUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $admin = new Admin();

        $admin->setEmail('true@test.com')
            ->setPassword('password');

        $this->assertTrue($admin->getEmail() === 'true@test.com');
        $this->assertTrue($admin->getPassword() === 'password');
    }

    public function testIsFalse()
    {
        $admin = new Admin();

        $admin->setEmail('true@test.com')
            ->setPassword('password');

        $this->assertFalse($admin->getEmail() === 'false@test.com');
        $this->assertFalse($admin->getPassword() === 'false');
    }

    public function testIsEmpty()
    {
        $admin = new Admin();

        $this->assertEmpty($admin->getEmail());
        $this->assertEmpty($admin->getPassword());
    }

}
