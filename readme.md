# ECF Médiathèque

## Description :

Afin de limiter la propagation du virus Covid-19 lors du premier confinement, certains établissements recevant du public, ont dû fermer leurs portes.
Inspirée par le click and collect, la médiathèque de La Chapelle-Curreaux souhaite développé en interne une solution d'emprunt en ligne.

[Lien vers le tableau de gestion du projet](https://trello.com/b/D7c3Mqe0/ecf-m%C3%A9diath%C3%A8que)

## Environnement de développement

### Pré-requis

- PHP 7.4.9
- composer
- Symfony CLI

vérifier les pré-requis avec la commande (CLI symfony) :

```
symfony check:requirements
```

### Installation du projet en local

* Cloner le projet
    ````git clone https://gitlab.com/chad2kay/ecf-mediatheque.git````

* Installation des dépendances
    ````composer install````
    ````npm install````

* Compilation du services webpack
    ````npm run build````

* executer les migrations
    ````php bin/console doctrine:migrations:migrate````

* créations des fixtures 
    ````php bin/console doctrine:fixtures:load --no-interaction````   

* Lancement du projet
    ````symfony serve -d```` 

### Déploiement du projet sur Heroku

* Initialisé un depot git 
    ````git init```` 

* Faire un premier commit
    ````git add -A````
    ````git commit -m "first commit"````

* Créer une application Heroku
    ````heroku create nom-de-l-app````

* Créer un fichier procfile à la racine du projet
    ```bash
    echo 'web: vendor/bin/heroku-php-apache2 public/' > Procfile
    git add Procfile
    git commit -m "Heroku Procfile"
    ```     

* Configurer la variable d'environnement Heroku
    ````heroku config:set APP_ENV=prod````

* Vérifier les parametres d'environnement sur la plateforme Heroku et ajouter les buildpacks node et php

* Deployer sur Heroku
    ````git push heroku master````

* Migrer les données de migrations
    ````heroku run php bin/console doctrine:migrations:migrate --no-interaction````

* Pour créer les fixtures, modifié le fichier bundels.php
    ````Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class => ['all' => true],````

* Puis lancer la commande suivante
    ````heroku run php bin/console doctrine:fixtures:load --no-interaction````

* Pour terminer
    ````heroku open````


## Identifiant de connexion

- Employé

|email   |mot de passe   |
|---|---|
| contact@e-mc2.com  |  password |




